import React from 'react';
import ReactDOM from 'react-dom/client';
import { ThemeProvider } from 'styled-components';
import {Content, Title, Card, Grid, Numbers, NewButton} from 'components';
import {configureStore, register} from 'core';
import {GlobalStyles, theme} from 'styles';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

const {persistor, store} = configureStore();

root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <Provider store={store}>
        <PersistGate persistor={persistor} loading={null}>
          <Content data-cy={'content'}>
            <Title data-cy={'title'}>Sudoku</Title>
            <Card data-cy={'card'}>
              <NewButton />
              <Grid/>
              <Numbers />
            </Card>
          </Content>
        </PersistGate>
      </Provider>
    </ThemeProvider>
  </React.StrictMode>
);

register();
